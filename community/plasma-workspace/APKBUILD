# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-workspace
pkgver=5.18.5
pkgrel=0
pkgdesc="KDE Plasma Workspace"
arch="all !armhf" # armhf blocked by kirigami2 -> qt5-qtdeclarative
url="https://www.kde.org/workspaces/plasmadesktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-or-later AND GPL-2.0-or-later AND MIT AND LGPL-2.1-only AND LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
depends="tzdata qt5-qtquickcontrols kirigami2 kinit qt5-qttools kwin kactivitymanagerd milou plasma-integration qtchooser kded kquickcharts kio-extras"
depends_dev="plasma-framework-dev krunner-dev kjsembed-dev knotifyconfig-dev kdesu-dev knewstuff-dev kwallet-dev kidletime-dev kdeclarative-dev ki18n-dev kcmutils-dev ktextwidgets-dev kdelibs4support-dev kcrash-dev kglobalaccel-dev kdbusaddons-dev kwayland-dev kcoreaddons-dev kded-dev libksysguard-dev kpackage-dev kscreenlocker-dev phonon-dev zlib-dev kitemmodels-dev networkmanager-qt-dev baloo-dev ktexteditor-dev kwin-dev kholidays-dev prison-dev kpeople-dev kactivities-stats-dev libkscreen-dev gpsd-dev iso-codes-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev libxtst-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# nightcolortest requires running dbus
	# testdesktop is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(nightcolortest|testdesktop)"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="870cf89649d9498831f4ef9b21d3c07504b7fc7b09b95dd7e0a1d356b41fbfceed1c4f27aa258bcf1e23cfe915d31701c155325fcd4944f9cc957a287ebc1ee2  plasma-workspace-5.18.5.tar.xz"
